#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <conio.h>

using namespace std;

static int N;

void input(vector<vector<int> > & tape)
{
    vector<int> temp;
    string line;
    cin>>N;
    cin.ignore(); // first iteration of loop ain't skipping
    for (int i=0;i<N;i++){
        start:
        getline(cin,line);
        int spaceCounter=0;
        for (int j=0;j<line.size();j++){
            if (line[j]==' '){
                spaceCounter++;
            }
            //cout<<spaceCounter<<endl;
        }
        if (spaceCounter==(N-1)){
            for (int j=0;j<line.size();j=j+2){
                if (line[j]!='1' && line[j]!='0'){
                    cout<<"Wrong numbers. Only 1 or 0 allowed. Try again.\n";
                    goto start;
                }
            }
            for (int j=0;j<line.size();j=j+2){
                temp.push_back(line[j] - '0');
            }
            tape.push_back(temp);
            temp.clear();
        } else {
            cout<<"Wrong number of spaces. Try again.\n";
            goto start;
        }
    }
}

void auto_input(vector<vector<int> > & tape)
{
    N=6;
    vector<int> a;
    vector<int> b;
    vector<int> c;
    vector<int> d;
    vector<int> e;
    vector<int> f;
   // vector<int> g;
a.push_back(0);a.push_back(0);a.push_back(0);a.push_back(0);a.push_back(0);a.push_back(1);
b.push_back(1);b.push_back(1);b.push_back(1);b.push_back(0);b.push_back(0);b.push_back(1);
c.push_back(0);c.push_back(0);c.push_back(1);c.push_back(1);c.push_back(1);c.push_back(1);
d.push_back(0);d.push_back(0);d.push_back(1);d.push_back(1);d.push_back(1);d.push_back(1);
e.push_back(1);e.push_back(1);e.push_back(1);e.push_back(1);e.push_back(0);e.push_back(1);
f.push_back(0);f.push_back(0);f.push_back(0);f.push_back(0);f.push_back(1);f.push_back(1);
//g.push_back(1);g.push_back(1);g.push_back(1);g.push_back(1);g.push_back(0);g.push_back(1);g.push_back(1);
    tape.push_back(a);
    tape.push_back(b);
    tape.push_back(c);
    tape.push_back(d);
    tape.push_back(e);
    tape.push_back(f);
    //tape.push_back(g);
}

void count(vector<vector<int> > & tape){
    int counter=0;
    int row_counter=0;
    int last_row_counter=0;
    for (int i=0;i<N;i++){
        end:
        last_row_counter=row_counter;
        if (i==0){ // if first row
            for (int j=0;j<N;j++){
                if (tape[i][j]==0){ // found zero
                    counter++; // global counter
                    row_counter++; // local counter
                    for (int k=j;k<N;k++){
                        if (tape[i][k]==1){ // found 1 after zero(es)
                            j=k; // remember position
                            break;
                        }
                        if (k==N-1){ // if there are no 1
                            i++; // next row
                            goto end;
                        }
                    }
                }
            }
        }
        if (i>0 && i<N){ // if other than first row
            int one_pos=0;
            int insertion=0;
            last_row_counter=row_counter;
            row_counter=0;
            for (int j=0;j<N;j++){
                if (tape[i][j]==0){ // found zero
                    counter++; // global counter
                    row_counter++; // local counter
                    for (int k=j;k<N;k++){
                        if (tape[i][k]==1){ // found 1 after zero(es)
                            one_pos=k; // remember position
                            break;
                        }
                        if (k==N-1)
                            one_pos=0; // there are no 1's
                    }
                    int zeroes=0;
                    if (one_pos){ // if 1 is found
                        for (int k=j;k<one_pos;k++){ // loop till 1
                            if (tape[i-1][k]==0){ // if there are zero above
                                insertion++;
                                for (int l=k;l<one_pos;l++){ // checking for squares
                                    if (tape[i-1][l]==0){
                                        zeroes++; // counting zeroes above
                                    }
                                }
                                if (zeroes==one_pos-k){ // if there are same amount of zeroes, it's square
                                    insertion=1;
                                    goto next;
                                }
                            }
                        }
                        j=one_pos; // to continue loop from 1's position
                    } else { // if there are no 1's
                        for (int k=j;k<N;k++){ // loop till end
                            if (tape[i-1][k]==0){ // if there are zero above
                                insertion++;
                                for (int l=k;l<N;l++){  // checking for squares %START
                                    if (tape[i-1][l]==0){
                                        zeroes++;  // counting zeroes above
                                    }
                                }
                                if (zeroes==N-k){ // if there are same amount of zeroes, it's square
                                    insertion=1;
                                    goto next;
                                }                       // %END
                            }
                        }
                    }
                    next:
                    if (insertion){ // if there are any insertion
                            counter-=insertion; // subtract them from counter
                    }
                    insertion=0;
                    if (!one_pos){ // if end of row
                        break;
                    }
                }
            }
        }
    }
    cout<<endl<<counter;
    getch();
    tape.clear(); // deallocating
}

void display(vector<vector<int> > & tape)
{
    for (int i=0;i<N;i++){
        for (int j=0;j<N;j++){
            cout<<tape[i][j];
        }
        cout<<endl;
    }
}


int main()
{
    vector<vector<int> > tape;
    input(tape);
    //auto_input(tape);
    //display(tape);
    count(tape);

    return 0;
}

/*
 * Как работает алгоритм: для начала вводится размерность массива, затем через string'овую переменную
 * считваются строки массива. После прохождения всех условий (кол-во пробелов в строке, число должно
 * быть 0 или 1) строка записывается в вектор. После введения всех данных мы имеем вектор, в каждой
 * ячейке которого содержится другой вектор. Подобие на двумерный массив. Алгоритм подсчета озер
 * основывается на "вхождениях". Считывание происходит один раз. В каждом ряду считается кол-во озер
 * не учитывая остальные строки, кроме верхней. Допустим, первая и вторая строка образуют у нас
 * такой массив: 00110110
 *               10000100
 * При считывании первой строки, алгоритм определяет три озера, при считывании второй (и больше) уже
 * работает система "вхождения" при обнаружении первого нуля считается сколько было вхождений в верхнюю
 * строку (когда на одной вертикали находятся нули) и отнимается от общего кол-ва озер. 3 озера было после
 * прохождения по первой строке, четвертое - нули на второй, кол-во вхождений - 2, в результате мы получаем
 * 2 озера; к концу второй строки переменная с кол-вом озер изменится до 3 и минус одной вхождение на
 * последней ячейке. Два озера. При переходе на следующую строку запоминается кол-во озер предыдущей, и
 * алгоритм работает по подобной схеме.
 * edit: добавил проверку на прямоугольные озёра. допустим, мы имеем такой массив: 010010
 *                                                                                 010010
 * после считывания первой строки, кол-во озёр у нас равняется трём. Когда цикл доходит до третьей ячейки
 * вектора, кол-во вхождений на участке с нулями равняется 2, хотя на самом деле для правильного результата
 * необходимо, чтобы для таких участков было одно вхождение. Проверка считает, равняется ли кол-во нулей на
 * текущем ряде кол-ву нулей на ряде выше, и меняет (или нет) кол-во вхождений. (этот участок обозначен в
 * коде комментариями %START и %END)
 */