#include <iostream>
#include <cctype>
#include <conio.h>
#include <string>
#include <cstdlib>
#include <vector>

using namespace std;
static int number, length;
static vector<int> VPositions; // vector with positions of gnomes
static vector<int> VDirections; // vector with directions of gnomes


void input_data()
{
    string data; // string for input number and length
    int SNumber_size;
    while (true){
        start:
        int spaceCounter=0;
        getline(cin, data);
        for (unsigned int i=0;i<data.length();i++){
            if (data[i]==' '){
                spaceCounter++;
                SNumber_size=i;
                int SLength_size=data.length()-SNumber_size;
                char SNumber[SNumber_size]; // array for string number
                char SLength[SLength_size]; // array for string length
                size_t SNumber_last_char = data.copy(SNumber,SNumber_size,0); // copying
                SNumber[SNumber_last_char]='\0';
                size_t SLength_last_char = data.copy(SLength,SLength_size,SNumber_size); // copying
                SLength[SLength_last_char]='\0';
                number = atoi(SNumber); // char array to int
                length = atoi(SLength); // char array to int
                if (spaceCounter>1){
                    cout<<"There are more than one space. Try again.\n";
                    goto start;
                }
                if (number<1 || number>105){
                    cout<<"Available range for number of gnomes is [1;105]. Try again.\n";
                    goto start;
                }
                if (length<2 || length>106){
                    cout<<"Available range for length is [1;106]. Try again.\n";
                    goto start;
                }
                if (number >= length ){
                    cout<<"Length of branch must be more than gnome counter.\n";
                    goto start;
                }
            }
        }
        if (!spaceCounter){
            cout<<"There are no spaces. Try again.\n";
            goto start;
        }
        if (spaceCounter==1){
            break;
        }
    }
}

void input_positions()
{
    string data; // string for input positions
    vector<int> VSpaceLocation; // vector for space locations
    while (true){
        start:
        unsigned int spaceCounter=0;
        getline(cin, data);
        for (unsigned int i=0;i<data.length();i++){
            if (data[i]==' '){
                spaceCounter++;
                VSpaceLocation.push_back(i); // pushing space location into vector
            }
        }
        if (spaceCounter!=number-1){
            cout<<"Wrong number of positions. Try again.\n";
            VSpaceLocation.clear();
            goto start;
        } else {
            VSpaceLocation.push_back(data.length()); // data.length() uses instead of last space
            int start=0, distance, end;
            for (unsigned int i=0;i<VSpaceLocation.size();i++)
            {
                    end=VSpaceLocation[i]; // end position of location
                    char SDistance[end-start]; // array for number location (size = end position - start position)
                    size_t s_tDistance = data.copy(SDistance,end-start,start); // copying position to array
                    SDistance[s_tDistance]='\0';
                    distance = atoi(SDistance); // char array to int
                    if (distance <1 || distance >= length){
                        cout<<"Wrong distance. [1;"<<length-1<<"] supports. Try again.\n";
                        VSpaceLocation.clear();
                        VPositions.clear();
                        goto start;
                    }
                    VPositions.push_back(distance);
                    start=end; // start takes place of an end position
            }
            break;
        }
    }
    VSpaceLocation.clear(); // deallocating
}


void input_directions()
{
    string data;
    vector<int> VSpaceLocation; // vector for space locations
    while (true){
        start:
        unsigned int spaceCounter=0;
        getline(cin, data);

        for (unsigned int i=0;i<data.length();i++){
            if (data[i]==' '){
                spaceCounter++;
                VSpaceLocation.push_back(i); // pushing space location into vector
            }
        }
        if (spaceCounter!=number-1){
            cout<<"Wrong number of directions. Try again.\n";
            VSpaceLocation.clear();
            goto start;
        } else {
            VSpaceLocation.push_back(data.length());
            int start=0, direction, end;
            for (unsigned int i=0;i<VSpaceLocation.size();i++)
            {
                    end=VSpaceLocation[i]; // end position of location
                    char SDistance[end-start]; // array for number location (size = end position - start position)
                    size_t s_tDistance = data.copy(SDistance,end-start,start); // copying direction to array
                    SDistance[s_tDistance]='\0';
                    direction = atoi(SDistance); // char array to int
                    if (direction != 1 && direction != -1){
                        cout<<"Wrong direction. Only 1 or -1 supports. Try again.\n";
                        VSpaceLocation.clear();
                        VDirections.clear();
                        goto start;
                    }
                    VDirections.push_back(direction);
                    start=end; // start takes place of an end position
            }
            break;
        }
    }
    VSpaceLocation.clear(); // deallocating
}

void move()
{
    vector<int> tempPositions;
    vector<int> tempDirections;
    int time=0;
    while(!VDirections.empty()){
        time++;
        tempDirections.clear();
        tempPositions.clear();
        for (int i=0;i<VDirections.size();i++){ // copying positions and directions to temporary vectors
            tempDirections.push_back(VDirections[i]);
            tempPositions.push_back(VPositions[i]);
            //cout<<"POS:"<<tempPositions[i]<<" ";
        }
        for (int i=0;i<VPositions.size();i++){
            if (VDirections[i]==1){ // if gnome goes right
                int next=VPositions[i]+1;
                for (int j=0;j<VPositions.size();j++){
                    if (VPositions[j]==next){ // if there are gnome nearby
                        if (VDirections[j]==1){ // if that gnome goes right
                            tempPositions[i]=VPositions[i]+1; // going right (temp vector)
                        } else { // if that gnome goes left
                            tempDirections[i]=-tempDirections[i]; // change direction of first gnome
                            tempDirections[j]=-tempDirections[j]; // and of the second one
                        }
                    } else { // if not
                        tempPositions[i]=VPositions[i]+1; // going right (temp vector)
                    }
                }
            } else if (VDirections[i]==-1){ // if gnome goes left
                int previous=VPositions[i]-1;
                for (int j=0;j<VPositions.size();j++){
                    if (VPositions[j]==previous){ // if there are gnome nearby
                        if (VDirections[j]==-1){ // if that gnome goes left
                            tempPositions[i]=VPositions[i]-1; // going left (temp vector)
                        } else { // if that gnome goes right
                            tempDirections[i]=-tempDirections[i]; // change direction of first gnome
                            tempDirections[j]=-tempDirections[j]; // and of the second one
                        }
                    } else { // if not
                        tempPositions[i]=VPositions[i]-1; // going left (temp vector)
                    }
                }
            }
        }
        for (int i=0;i<tempPositions.size();i++){
            for (int j=i+1;j<tempPositions.size();j++){
                if (tempPositions[i]==tempPositions[j]){ // if there are gnomes standing on the same cell
                    tempDirections[i]=-tempDirections[i]; // changing direction of first gnome
                    tempDirections[j]=-tempDirections[j]; // and of the second one
                }
            }
        }
        VDirections.clear(); // deallocating
        VPositions.clear(); // deallocating
        for (int i=0;i<tempPositions.size();i++){
            VDirections.push_back(tempDirections[i]); //
            VPositions.push_back(tempPositions[i]); // writing changes from temp vectors
        }
        int temp=0;
        for (vector<int>::iterator it = VPositions.begin();it!=VPositions.end(); /*EMPTY*/ ){
            if (*it==0||*it==length){                                 // if position = begin or end
                if (VPositions.size()>1){                             //
                    it = VPositions.erase(it);                        // erase positions
                    VDirections.erase(VDirections.begin()+temp);      // erase direction
                }
                else {                                                // if element is last in vector
                    VPositions.clear();                               // clear both vectors
                    VDirections.clear();
                    break;
                }

            } else {
                ++it;
                temp++;
            }
        }

       /* int AOutput[length];
        for (int i=0;i<length;i++){
                for (int j=0;j<VPositions.size();j++){
                    if (i==VPositions[j]){
                        AOutput[i]=1;
                        break;
                    } else {
                        AOutput[i]=0;
                    }
                }
                cout<<AOutput[i];
        }
        cout<<endl;*/

    }
    VDirections.clear();    // --
    VPositions.clear();     // --
    tempDirections.clear(); // deallocation
    tempPositions.clear();  // --
    cout<<endl<<time;
    getch();
}

int main()
{
    input_data();
    input_positions();
    input_directions();
    move();
    return 0;
}

/*
 * Как работает алгоритм: Для начала, в первой строке, через пробел вводится кол-во гномов
 * и длинна лавочки. Условные опрераторы проверяют строку на правильность ввода данных
 * (должно быть два числа определенного диапазона, а кол-во гномов меньше длинны лавочки)
 * После проверки, строка разбивается на две пробелом, а числа переводятся в чисельный тип.
 * Далее следует стадия ввода позиции. Условный оператор считает кол-во пробелов,
 * и определяет введено ли правильное кол-во позиций. Позиции пробелов записваются в вектор,
 * а затем используются в цикле как промежуточные позиции между числами. Условные опраторы
 * позволяют ввести данные, которые не выходят за заданный диапазон. По такой-же схеме работает
 * этап ввода направления. Для движения гномов задействовано еще два дополнительных временных вектора.
 * Позиции и направления копируются во временные векторы. Затем идет первый шаг. Цикл проходит по вектору
 * направлений, и проверяет, гном идёт влево или вправо. После этого проводится проверка соседней позиции,
 * если на ней стоит гном, то определяется его направление, и уже после этого меняются данные первого гнома -
 * если направления совпадают, во временной вектор заносится изменение его позиции в сторону его направления.
 * Если направления различаются, у первого и второго гнома во временном векторе изменяется направление на
 * противоположенное (аля отталкиваются). После такой процедуры может сложиться такая ситуация, что два гнома
 * в один и тот же момент стоят на одной и той же позиции. Циклично временные векторы проверяются на существование
 * таких ситуаций, если такие находятся, у обоих гномов меняется направление (аля отталкиваются на позиции).
 * Теперь мы имеем временный вектор с позициями и направлениями после одной итерации (шага). Оригинальные
 * векторы очищаются, в них записываются данные с временных векторов. Последний цикл проверяет
 * векторы на наличие гномов с положением 0 или конец лавочки, и если такие существуют, стирают эту запись с
 * обоих векторов. Если запись является последней, очищается весь вектор (иначе итератор будет указывать на
 * несуществующий элемент.
 */